# mugui-spring

#### 介绍
超轻量级分布式框架，使用场景：中小型项目，快速开发，简单，无学习成本。

* 支持多数据库。
* 支持bean与数据库表双向生成。
* 支持json与bean之间快速装换。
* 支持不同bean之间的快速转换与装载。
* 支持缓存。
* 支持微服务，支持转发，多模块快速开发。
* 支持权限控制。
* 支持日志监听。
* 复杂操作简单化，快速开发。


#### 安装教程

```
<dependency>
  <groupId>cn.net.mugui</groupId>
  <artifactId>spring</artifactId>
  <version>{version}</version>
</dependency>

```
## 使用说明
#### 编辑中

更多方法查看api,或访问示例项目：
