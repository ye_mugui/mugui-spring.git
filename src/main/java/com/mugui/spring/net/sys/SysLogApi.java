package com.mugui.spring.net.sys;

import org.apache.dubbo.rpc.service.GenericService;

import com.mugui.spring.net.bean.NetBag;

public interface SysLogApi extends GenericService {
	void info(String application_name,String msg);

	void info(String application_name,String msg, Exception exception);

	void error(String application_name,Exception exception);

	void error(String application_name,String msg);

	void error(String application_name,String msg, Exception exception);

	void warn(String application_name,String msg);

	void warn(String application_name,String msg, Exception exception);

	void info(String application_name,NetBag bag);

	void info(String application_name,NetBag bag, Object msg);

	void info(String application_name,NetBag bag, Object msg, Exception exception);

	void error(String application_name,NetBag bag);

	void error(String application_name,NetBag bag, Exception exception);

	void error(String application_name,NetBag bag, Object msg, Exception exception);

	void warn(String application_name,NetBag bag);

	void warn(String application_name,NetBag bag, Object msg);

	void warn(String application_name,NetBag bag, Object msg, Exception exception);

}
