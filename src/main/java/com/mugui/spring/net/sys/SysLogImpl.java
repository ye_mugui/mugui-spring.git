package com.mugui.spring.net.sys;

import org.apache.dubbo.rpc.service.GenericException;
import org.apache.dubbo.rpc.service.GenericService;

import com.mugui.Mugui;
import com.mugui.spring.net.bean.NetBag;

public class SysLogImpl implements SysLogApi, Mugui {

	private GenericService genericService;

	public SysLogImpl(GenericService genericService) {
		this.genericService = genericService;
	}

	@Override
	public Object $invoke(String method, String[] parameterTypes, Object[] args) throws GenericException {
		try {
			return genericService.$invoke(method, parameterTypes, args);
		} catch (Exception e) {
			throw new GenericException(e);
		}
	}

	@Override
	public void info(String application_name, String msg) {
		info(application_name, msg, null);
	}

	@Override
	public void info(String application_name, String msg, Exception exception) {
		handleString(application_name, Thread.currentThread().getStackTrace()[1].getMethodName(), msg, exception);
	}

	@Override
	public void error(String application_name, Exception exception) {
		handleString(application_name, Thread.currentThread().getStackTrace()[1].getMethodName(), null, exception);
	}

	@Override
	public void error(String application_name, String msg) {
		error(application_name, msg, null);

	}

	@Override
	public void error(String application_name, String msg, Exception exception) {
		handleString(application_name, Thread.currentThread().getStackTrace()[1].getMethodName(), msg, exception);

	}

	@Override
	public void warn(String application_name, String msg) {
		warn(application_name, msg, null);
	}

	@Override
	public void warn(String application_name, String msg, Exception exception) {
		handleString(application_name, Thread.currentThread().getStackTrace()[1].getMethodName(), msg, exception);

	}

	private void handleString(String application_name, String methodName, String msg, Exception exception) {
		if (msg != null && msg.toString().length() >= 1024) {
			msg = msg.toString().substring(0, 1023);
		}
		genericService.$invoke(methodName,
				new String[] { String.class.getName(), String.class.getName(), Exception.class.getName() },
				new Object[] { application_name, msg, exception });
	}

	private void handleNetbag(String application_name, String methodName, NetBag bag, Object msg, Exception exception) {
		if (msg != null && msg.toString().length() >= 1024) {
			msg = msg.toString().substring(0, 1023);
		}
		genericService
				.$invoke(
						methodName, new String[] { String.class.getName(), NetBag.class.getName(),
								Object.class.getName(), Exception.class.getName() },
						new Object[] { application_name, bag, msg, exception });
	}

	@Override
	public void info(String application_name, NetBag bag) {
		info(application_name, bag, null);
	}

	@Override
	public void info(String application_name, NetBag bag, Object msg) {
		info(application_name, bag, msg, null);
	}

	@Override
	public void info(String application_name, NetBag bag, Object msg, Exception exception) {
		handleNetbag(application_name, Thread.currentThread().getStackTrace()[1].getMethodName(), bag, msg, exception);
	}

	@Override
	public void error(String application_name, NetBag bag) {
		error(application_name, bag, null);

	}

	@Override
	public void error(String application_name, NetBag bag, Object msg, Exception exception) {
		handleNetbag(application_name, Thread.currentThread().getStackTrace()[1].getMethodName(), bag, msg, exception);

	}

	@Override
	public void error(String application_name, NetBag bag, Exception exception) {
		error(application_name, bag, null, exception);
	}

	@Override
	public void warn(String application_name, NetBag bag) {
		warn(application_name, bag, null);
	}

	@Override
	public void warn(String application_name, NetBag bag, Object msg) {
		warn(application_name, bag, msg, null);

	}

	@Override
	public void warn(String application_name, NetBag bag, Object msg, Exception exception) {
		handleNetbag(application_name, Thread.currentThread().getStackTrace()[1].getMethodName(), bag, msg, exception);
	}
}
