package com.mugui.spring.net.listener;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import com.mugui.spring.base.Listener;
import com.mugui.spring.base.ManagerInterface;
import com.mugui.spring.net.classutil.DataSave;


@Component
public class ListenerManager implements ManagerInterface<String, ListenerModel> {
	private static HashMap<String, ListenerModel> map = null;

	public void init() {
		clear();
		map = new HashMap<>();
	}

	public Set<Entry<String, ListenerModel>> entrySet() {
		
		return map.entrySet();
	}

	private ApplicationContext applicationContext = null;
	@Override
	public boolean init(Object object) {
		Class<?> name=(Class<?>) object;
		init();
		applicationContext = (ApplicationContext) System.getProperties().get("Application");
		for (Class<?> class_name : DataSave.initClassList(name)) {
			if (class_name.isAnnotationPresent(Listener.class)) {
				System.out.println("监听器：" + class_name.getName());
				Listener filter = class_name.getAnnotation(Listener.class);
				map.put(filter.hashCode() + "", (ListenerModel) applicationContext.getBean(class_name));
			}
		}
	
		return false;
	}

	@Override
	public boolean clear() {
		if (map != null)
			map.clear();
		return true;
	}

	@Override
	public boolean is(String name) {
		if (map == null) {
			throw new NullPointerException("please run init");
		}
		return !map.isEmpty() && map.get(name) != null;
	}

	@Override
	public ListenerModel del(String name) {
		if (map == null) {
			throw new NullPointerException("please run init");
		}
		return map.remove(name);
	}

	@Override
	public ListenerModel get(String name) {
		if (map == null) {
			throw new NullPointerException("please run init");
		}
		return map.get(name);
	}

	@Override
	public boolean add(String name, ListenerModel object) {
		if (map == null) {
			throw new NullPointerException("please run init");
		}
		return true;

	}

	@Override
	public boolean isInit() {
		// TODO Auto-generated method stub
		return false;
	}



}
