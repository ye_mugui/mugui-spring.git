package com.mugui.spring.net.listener;

import com.mugui.Mugui;
import com.mugui.spring.net.bean.Message;
import com.mugui.spring.net.bean.NetBag;

public interface ListenerModel extends Mugui {
	public  void listener(Message message, NetBag bag);

}
