package com.mugui.spring.net.baghandle;

import java.io.FilePermission;
import java.security.Permission;
import java.util.HashMap;

import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import com.mugui.Mugui;
import com.mugui.spring.base.ModelInterface;
import com.mugui.spring.base.ModelManagerInterface;
import com.mugui.spring.base.Module;
import com.mugui.spring.base.TaskInterface;
import com.mugui.spring.net.auto.AutoLoadManager;
import com.mugui.spring.net.auto.AutoTask;
import com.mugui.spring.net.cache.CacheMessage;
import com.mugui.spring.net.classutil.DataSave;
import com.mugui.spring.net.filter.FilterManager;
import com.mugui.spring.net.listener.ListenerManager;
import com.mugui.spring.net.task.TaskManager;

import lombok.Getter;

@Component
public class NetBagModuleManager implements ModelManagerInterface {

	private static HashMap<String, ModelInterface> map = null;
	@Getter
	private FilterManager filterMessage = null;
	@Getter
	private ListenerManager listenerMessage = null;
	@Getter
	private CacheMessage cacheMessage = null;

	public void init() {
		SecurityManager securityManager = System.getSecurityManager();
		if (securityManager == null) {
			securityManager = new SecurityManager() {
				private void check(Permission perm) {
					// 禁止exec
					if (perm instanceof java.io.FilePermission) {
						String actions = perm.getActions();
						if (actions != null && actions.contains("execute")) {
							throw new SecurityException("execute denied! " + perm.getName());
						}
					}
					// 禁止设置新的SecurityManager，保护自己
					if (perm instanceof java.lang.RuntimePermission) {
						String name = perm.getName();
						if (name != null && name.contains("setSecurityManager")) {
							throw new SecurityException("System.setSecurityManager denied!");
						}
					}
				}

				public void checkExec(String cmd) {
					checkPermission(new FilePermission(cmd, "execute"));
				}

				@Override
				public void checkPermission(Permission perm) {
					check(perm);
				}

				@Override
				public void checkPermission(Permission perm, Object context) {
					check(perm);
				}
			};
			System.setSecurityManager(securityManager);
		} else {
			System.err.println("已加载自定义安全管理器：" + securityManager);
		}
		clear();
		map = new HashMap<>();

	}

	public <T> T getModelManager(Class<T> class1) {
		return applicationContext.getBean(class1);
	}

	private ApplicationContext applicationContext = null;

	@Override
	public boolean init(Object o) {
		Class<?> name = (Class<?>) o;
		init();
		TaskManager manager = TaskManager.getTaskManager();
		manager.init();
		applicationContext = (ApplicationContext) System.getProperties().get("Application");
		filterMessage = applicationContext.getBean(FilterManager.class);
		filterMessage.init(name);
		listenerMessage = applicationContext.getBean(ListenerManager.class);
		listenerMessage.init(name);
		cacheMessage = applicationContext.getBean(CacheMessage.class);
		cacheMessage.init(name);
		applicationContext.getBean(AutoLoadManager.class).init(name);
		for (Class<?> class_name : DataSave.initClassList(name)) {
			if (class_name.isAnnotationPresent(Module.class)) {
				try {
					System.out.println("module加载：" + class_name.getName());
					handleModule(class_name);
				} catch (Exception e) {
					e.printStackTrace();
					throw new RuntimeException(e);
				}
			} else if (class_name.getSuperclass() != null
					&& class_name.getSuperclass().isAnnotationPresent(Module.class)) {
				try {
					System.out.println("module加载：" + class_name.getSuperclass());
					handleModule(class_name.getSuperclass());
				} catch (Exception e) {
					e.printStackTrace();
					throw new RuntimeException(e);
				}
			}
			if (class_name.isAnnotationPresent(AutoTask.class)) {
				TaskInterface interface1 = (TaskInterface) applicationContext.getBean(class_name);
				try {
					interface1.invokeFunction("init");
				} catch (Exception e) {
					e.printStackTrace();
				}
				manager.add(interface1);
			}
		}
		System.getProperties().put("system_lock", false);

		return true;
	}

	/**
	 * 处理模块
	 * 
	 * @param class_name
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws Exception
	 */

	private void handleModule(Class<?> class_name) throws InstantiationException, IllegalAccessException, Exception {

		if (!Mugui.class.isAssignableFrom(class_name)) {
			throw new RuntimeException("Module  not implements Mugui of " + class_name.getName());
		}
		Module module = class_name.getAnnotation(Module.class);
		ModelInterface modelInterface = null;
		modelInterface = map.get(module.name() + "." + module.type());
		if (modelInterface != null) {
//			throw new RuntimeException("module :" + module.name() + "." + module.type() + " is already defined of "
//					+ class_name.getName());
			return;
		}
		if ((modelInterface = map.get(module.name() + "." + module.type())) == null) {
			modelInterface = new NetBagModule();
		}
		modelInterface.invokeFunction("init", module, class_name.newInstance());
		map.put(module.name() + "." + module.type(), modelInterface);

	}

	@Override
	public boolean clear() {
		if (map != null)
			map.clear();
		return true;
	}

	@Override
	public boolean is(String name) {
		if (map == null) {
			throw new NullPointerException("please run init");
		}
		return !map.isEmpty() && map.get(name) != null;
	}

	@Override
	public ModelInterface del(String name) {
		if (map == null) {
			throw new NullPointerException("please run init");
		}
		return map.remove(name);
	}

	@Override
	public ModelInterface get(String name) {
		if (map == null) {
			throw new NullPointerException("please run init");
		}
		return map.get(name);
	}

	@Override
	public boolean add(String name, ModelInterface object) {
		if (map == null) {
			throw new NullPointerException("please run init");
		}
		return true;
	}

	@Override
	public boolean isInit() {
		// TODO Auto-generated method stub
		return false;
	}

}
