package com.mugui.spring.net.baghandle;

import com.mugui.spring.net.bean.NetBag;

/**
 * 模块状态监听器
 * 
 * @author 木鬼
 *
 */
public interface ModuleListenerServiceApi {
 
	void time(String application_name,String func, long l);

	void listener(String application_name, NetBag bag, String string, Exception e);
}
