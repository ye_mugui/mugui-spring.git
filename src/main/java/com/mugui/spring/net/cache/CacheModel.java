package com.mugui.spring.net.cache;

import com.mugui.Mugui;
import com.mugui.spring.net.bean.Message;
import com.mugui.spring.net.bean.NetBag;

public interface CacheModel extends Mugui {
	public NetBag load(NetBag bag);

	public void save(Message message, NetBag bag);
}
