package com.mugui.spring.net.cache;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import com.mugui.spring.base.Cache;
import com.mugui.spring.base.ManagerInterface;
import com.mugui.spring.net.classutil.DataSave;

@Component
public class CacheMessage implements ManagerInterface<String, CacheModel> {
	private static HashMap<String, CacheModel> map = null;

	public void init() {
		clear();
		map = new HashMap<>();
	}

	public Set<Entry<String, CacheModel>> entrySet() {
		return map.entrySet();
	}

	private ApplicationContext applicationContext = null;

	@Override
	public boolean init(Object object) {
		Class<?> name = (Class<?>) object;
		init();
		applicationContext = (ApplicationContext) System.getProperties().get("Application");
		for (Class<?> class_name : DataSave.initClassList(name)) {
			if (class_name.isAnnotationPresent(Cache.class)) {
				Cache filter = class_name.getAnnotation(Cache.class);
				map.put(filter.hashCode() + "", (CacheModel) applicationContext.getBean(class_name));
			}
		}

		return false;
	}

	@Override
	public boolean clear() {
		if (map != null)
			map.clear();
		return true;
	}

	@Override
	public boolean is(String name) {
		if (map == null) {
			throw new NullPointerException("please run init");
		}
		return !map.isEmpty() && map.get(name) != null;
	}

	@Override
	public CacheModel del(String name) {
		if (map == null) {
			throw new NullPointerException("please run init");
		}
		return map.remove(name);
	}

	@Override
	public CacheModel get(String name) {
		if (map == null) {
			throw new NullPointerException("please run init");
		}
		return map.get(name);
	}

	@Override
	public boolean add(String name, CacheModel object) {
		if (map == null) {
			throw new NullPointerException("please run init");
		}
		return true;
	}

	@Override
	public boolean isInit() {
		// TODO Auto-generated method stub
		return false;
	}

}
