package com.mugui.spring.net.bean;

import com.mugui.bean.JsonBean;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class NetBag extends JsonBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8224372157110570864L;

	/**
	 * http连接类型
	 */
	public static final int TYPE_HTTP = 2;

	/**
	 * upd连接类型
	 */
	public static final int TYPE_UDP = 1;
	/**
	 * WS连接类型
	 */
	public static final int TYPE_WS = 3;
	/**
	 * 隐形参数：文件上传
	 */
	public static final String STEALTH_UPLOAD_FILE = "uploadFile";
	
	/**
	 * 时间戳
	 */
	private String timestamp;
	private String host;
	private Integer port;

	/**
	 * 来至哪个host、若包没有被转发则，默认为自己
	 */
	private String from_host;

	/**
	 * 来至哪个端口的消息、若包没有被转发，则为本地绑定的端口号
	 */
	private Integer from_port;

	/**
	 * 网络连接状态:101超时 200 成功 404 资源错误 503发生异常
	 */
	private Integer code;
	/**
	 * 包唯一hash
	 */
	private String hash;
	/**
	 * 服务器类型
	 */
	private String server_type;

	/**
	 * 类型1.udp连接过来的2.http连接过来的
	 */
	private Integer type;

	/**
	 * 访问的资源连接处理函数名由：模块名.操作类型({@link Module}).函数名组成
	 */
	private String func;

	/**
	 * 数据处理主体
	 */
	private Object data;

	/**
	 * 反馈给其他函数进行处理的参数，并不会在返回客户端
	 */
	private Object ret_data;

	/**
	 * 用户唯一标示
	 * 
	 */
	private String session;

}
