package com.mugui.spring.net.bean;

import java.util.Map;

import com.mugui.bean.JsonBean;
import com.mugui.spring.net.dblistener.DBListener;
import com.mugui.sql.SQLDB;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
/**
 * 异常信息类
 * 
 * @author HayDen 2019-05-21 18:10
 */
@Getter
@Setter
@Accessors(chain = true)
public class Message extends JsonBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4374299159671270375L;
	public static final int SUCCESS = 200;// 成功
	public static final int LOGOUT = 201;
	/**
	 * 处理中
	 */
	public static final int PROCESSING = 202;
	public static final int ERROR = 503;// 失败
	public static final String MAG = "操作成功";// 成功
	public static final String FAIL = "操作失败";// 失败

	public static Message ok() {
		Message message = new Message();
		message.setMsg("成功");
		message.setType(Message.SUCCESS);
		message.setExtra(Message.MAG);
		return message;
	}

	public static Message processing(String string) {
		Message message = new Message();
		message.setMsg("消息处理中");
		message.setType(Message.PROCESSING);
		message.setExtra(string);
		return message;
	}

	public static Message ok(Object date, String msg) {
		Message message = new Message();
		if (date != null) {
			if (date instanceof JsonBean)
				message.setDate(((JsonBean) date).toJson());
			else if (date instanceof Map) {
				message.setDate(com.alibaba.fastjson.JSONObject.toJSON(date));
			} else {
				message.setDate(date); 
			}
		}
		message.setType(Message.SUCCESS);
		message.setMsg(msg);
		message.setExtra(Message.MAG);
		return message;
	}

	public static Message ok(Object date) {
		return ok(date, null);
	}

	public static Message error(String msg) {
		Message message = new Message();
		message.setType(Message.ERROR);
		message.setExtra(Message.FAIL);
		message.setMsg(msg);
		return message;
	}

	/**
	 * 登录拦截
	 * 
	 * @return
	 */
	public static Message logout() {
		Message message = new Message();
		message.setType(Message.LOGOUT);
		message.setExtra("session发生变化，已登出");
		return message;
	}

	/**
	 * 类型
	 */
	private int type;

	/**
	 * 消息简述
	 */
	private String msg;

	/**
	 * 描述
	 * 
	 * @return
	 */
	private String extra;

	/**
	 * 返回的参数
	 */
	private Object date;



}
