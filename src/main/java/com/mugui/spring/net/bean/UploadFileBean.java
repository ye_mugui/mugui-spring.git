package com.mugui.spring.net.bean;

import com.mugui.bean.JsonBean;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class UploadFileBean extends JsonBean {
	private String name;
	private String file;
	private String type;
}
