package com.mugui.spring.net.auto;

import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import com.mugui.bean.JsonBean;
import com.mugui.spring.base.ManagerInterface;
import com.mugui.spring.net.classutil.DataSave;
import com.mugui.sql.SqlModel;

/**
 * 自动创建表管理
 * @author Administrator
 *
 */
@Component
@AutoManager
public class AutoCreateManager implements ManagerInterface<Object,Object> {
	private SqlModel SqlModel;

	public void init() {
		clear();
		SqlModel = new SqlModel();
	}

	private ApplicationContext applicationContext = null;

	public void init(Class<?> name) throws Exception {
		init();
		applicationContext = (ApplicationContext) System.getProperties().get("Application");
		for (Class<?> class_name : DataSave.initClassList(name)) {
			if (class_name.isAnnotationPresent(AutoCreateTable.class)) {
				AutoCreateTable filter = class_name.getAnnotation(AutoCreateTable.class);
				SqlModel.createTable((Class<? extends JsonBean>) class_name);
			}
		}
	}

	@Override
	public boolean isInit() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean init(Object object) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Object get(Object key) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean add(Object key, Object value) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean is(Object key) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Object del(Object key) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean clear() {
		// TODO Auto-generated method stub
		return false;
	}



}
