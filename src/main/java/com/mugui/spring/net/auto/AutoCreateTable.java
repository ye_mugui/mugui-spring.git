package com.mugui.spring.net.auto;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * 自动创建表
 * @author Administrator
 *
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface AutoCreateTable {

	String value(); 

}
