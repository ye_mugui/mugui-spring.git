package com.mugui.spring.net.forward;

import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;
import com.mugui.spring.net.bean.NetBag;
import com.mugui.spring.net.filter.FilterModel;
import com.mugui.spring.util.HTTPUtil;

/**
 * 已失效
 * @author Administrator
 *
 */
@Deprecated
@Component
public class ForwardController implements FilterModel {
	private ForwardManager forwardManager;

	@Override
	public NetBag filter(NetBag bag) {
		Forward forward = (Forward) forwardManager.get(bag.getFunc());
		if (forward == null) {
			return bag;
		}
		bag.InitBean(JSONObject.parseObject(HTTPUtil.post(forward.url(), bag.toString())));
		return null;
	}



}
