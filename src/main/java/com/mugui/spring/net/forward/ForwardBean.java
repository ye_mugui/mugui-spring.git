package com.mugui.spring.net.forward;

import com.mugui.bean.JsonBean;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.Getter;

@Getter
@Setter
@Accessors(chain = true)
public class ForwardBean extends JsonBean {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5947437048851152234L;
	private String url;
	private String value;

	/**
	 * 需要被注入的websockt监听消息
	 */
	private String ws;
	
	private String md5;
}
