package com.mugui.spring.net.forward;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.mugui.Mugui;
/**
 * 
 * @author 木鬼
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Inherited
public @interface Forward {

	public String url();

	/**
	 * 向目标服务器群注入这个url拦截请求
	 */
	public String[] inject() default {};
	
	/*
	 *************************一下2者参数不可同时为空************************
	 */
	/**
	 * 可被转发的消息
	 */
	public String[] value() default {};
	/**
	 * 需要被注入的拥有Module注解的类
	 */
	public Class<Mugui>[] classes() default Mugui.class;
}
