package com.mugui.spring.net.authority;

import java.util.Date;

import com.mugui.bean.JsonBean;
import com.mugui.sql.SQLDB;
import com.mugui.sql.SQLField;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * 权限表
 * 
 * @author 木鬼
 *
 */
@SQLDB(TABLE = "authority", KEY = "authority_id")
@Getter
@Setter
@Accessors(chain = true)
public class AuthorityBean extends JsonBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7468107253238167919L;
	@SQLField(PRIMARY_KEY = true, AUTOINCREMENT = true)
	private Integer authority_id;
	@SQLField(NULL = false, DATA_TYPE = "varchar(64)", UNIQUE = true)
	private String authority_key;
	@SQLField(NULL = false, DEFAULT = true, DEFAULT_text = "0")
	private Integer authority_value;

	@SQLField(DATA_TYPE = "varchar(125)")
	private String authority_info;

	@SQLField(DEFAULT = true, DEFAULT_text = "CURRENT_TIMESTAMP")
	private Date authority_create_time;

	@SQLField()
	private Date authority_update_time;

}
