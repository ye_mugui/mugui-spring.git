package com.mugui.spring.net.authority;

import com.mugui.Mugui;
import com.mugui.spring.net.bean.Message;
import com.mugui.spring.net.bean.NetBag;

public interface AuthorityServiceApi extends Mugui {

	Message filter(NetBag bag);

}
