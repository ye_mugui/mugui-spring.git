package com.mugui.spring.net.authority;

import org.apache.dubbo.rpc.service.GenericService;

public interface AuthorityManagerApi extends GenericService {

	void addAuthority(String key, boolean value);
	
	Object getAuthority(String key);
}
