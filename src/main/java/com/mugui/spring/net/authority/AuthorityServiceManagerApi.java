package com.mugui.spring.net.authority;

import org.apache.dubbo.rpc.service.GenericService;

import com.mugui.bean.JsonBean;
import com.mugui.spring.net.bean.NetBag;

public interface AuthorityServiceManagerApi extends GenericService {
	JsonBean filter(NetBag bag);
}
