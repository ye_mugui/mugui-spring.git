package com.mugui.spring.net.authority;

import java.util.HashMap;

import org.apache.dubbo.config.ReferenceConfig;
import org.apache.dubbo.config.annotation.Reference;
import org.apache.dubbo.rpc.service.GenericService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import com.mugui.spring.base.ManagerInterface;
import com.mugui.spring.base.Module;
import com.mugui.spring.net.authority.Authority;
import com.mugui.spring.net.auto.AutoManager;
import com.mugui.spring.net.classutil.DataSave;
import com.mugui.sql.SqlModel;

/**
 * 权限管理器，用于快速判断是否有func访问权限
 * 
 * @author 木鬼
 *
 */
@AutoManager
public class AuthorityInitManager implements ManagerInterface<String, Object> {

	@Override
	public boolean isInit() {
		return false;
	}

	@Value("${mugui.spring.authority.open:false}")
	private boolean isAuthority;

	@Override
	public boolean init(Object object) {
		if (!isAuthority)
			return isAuthority;
		init();
		sqlModel = new SqlModel();
		readSpringYml();
		if (!open)
			return open;
		if (byclass)
			loadAuthorityByClass((Class<?>) object);
		return false;
	}

	private void init() {
		if (authorityManagerApi == null) {
			ReferenceConfig<GenericService> referenceConfig = new ReferenceConfig<>();
			referenceConfig.setVersion("");
			referenceConfig.setInterface(AuthorityManagerApi.class);
			referenceConfig.setGroup("default");
			referenceConfig.setGeneric(true);
			authorityManagerApi = referenceConfig.get();
		}

	}

	private Boolean open = null;
	private Boolean byclass = null;

	@Autowired
	private Environment environment;

	/**
	 * 可能运行与springboot框架下，则从该框架中读取基本配置<br>
	 * 暂只支持application.yml
	 * 
	 * @auther 木鬼
	 */
	private void readSpringYml() {
		if (open == null)
			try {
				open = environment.getProperty("mugui.spring.authority.open", Boolean.class);
				if (open == null) {
					open = false;
				}
				byclass = environment.getProperty("mugui.spring.authority.byclass", Boolean.class);
				if (byclass == null) {
					byclass = false;
				}
			} catch (Exception e1) {
				e1.printStackTrace();
				open = false;
				byclass = false;
				throw new RuntimeException("读取Authority 配置错误", e1);
			}
	}

	private GenericService authorityManagerApi;

	/**
	 * 从class中加载权限管理
	 * 
	 * @auther 木鬼
	 * @param object
	 */
	private void loadAuthorityByClass(Class<?> object) {
		authorityManagerApi.$invoke("init", new String[] { String.class.getName() }, new Object[] { null });
		for (Class<?> class_name : DataSave.initClassList(object)) {
			if (class_name.isAnnotationPresent(Authority.class)) {
				Authority annotation = class_name.getAnnotation(Authority.class);
				boolean annotationPresent = class_name.isAnnotationPresent(Module.class);
				if (!annotationPresent) {
					throw new RuntimeException("此注解是Module 注解的补充注解，需注解在Module下");
				}
				Module annotation2 = class_name.getAnnotation(Module.class);
				String key = annotation2.name() + "." + annotation2.type() + ".*";
				authorityManagerApi.$invoke("addAuthority",
						new String[] { String.class.getName(), Boolean.class.getName() },
						new Object[] { key, annotation.value() });
			}
		}
	}

	SqlModel sqlModel = null;
	private HashMap<String, Object> map = new HashMap<String, Object>();

	@Override
	public boolean is(String key) {
		return false;
	}

	@Override
	public boolean clear() {
		return false;
	}

	/**
	 * 禁止实现此方法
	 */
	@Deprecated
	@Override
	public Object del(String key) {
		return null;
	}

	@Override
	public Object get(String key) {
		return null;
	}

	@Override
	public boolean add(String key, Object value) {
		return false;
	}

}
