package com.mugui.spring.net.lock;

import com.mugui.spring.base.Filter;
import com.mugui.spring.base.Listener;
import com.mugui.spring.net.bean.Message;
import com.mugui.spring.net.bean.NetBag;
import com.mugui.spring.net.filter.FilterModel;
import com.mugui.spring.net.listener.ListenerModel;
import com.mugui.spring.net.session.SessionContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpSession;

import java.util.HashMap;
import java.util.concurrent.locks.ReentrantLock;

@Component
@Filter(weight = 98, value = { "*" }, type = Filter.POSITIVE)
@Listener(weight = 98, value = { "*" }, type = Filter.POSITIVE)

public class SynchronizeLock implements FilterModel, ListenerModel {
	public static byte[] lock = new byte[0];

	public static HashMap<String, ReentrantLock> map = null;

	@Override
	public NetBag filter(NetBag bag) {
		return lock(bag);

	}

	@Autowired
	private GroupLockManager groupLockManager = null;

	private NetBag lock(NetBag bag) {
		String name = (String) groupLockManager.get(bag.getFunc());
		if (name == null)
			return bag;

		ReentrantLock key = null;
		HttpSession session = SessionContext.getSession(bag.getSession());
		synchronized (lock) {
			key = (ReentrantLock) session.getValue(name);
			if (key == null) {
				session.putValue(name, key = new ReentrantLock());
			}
		}
		if (key.isLocked())
			return null;
		key.lock();

		return bag;
	}

	@Override
	public void listener(Message message, NetBag bag) {
		String name = (String) groupLockManager.get(bag.getFunc());
		if (name == null)
			return;
		HttpSession session = null;
		if (null == (session = SessionContext.getSession(bag.getSession()))) {
			return;
		}
		ReentrantLock object = (ReentrantLock) session.getValue(name);
		if (object != null) {
			if (object.isHeldByCurrentThread())
				object.unlock();
		}

	}

}
