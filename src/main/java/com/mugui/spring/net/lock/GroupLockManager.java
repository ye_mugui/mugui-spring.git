package com.mugui.spring.net.lock;

import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.Set;
import java.util.Map.Entry;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.mugui.spring.base.ManagerInterface;
import com.mugui.spring.net.auto.AutoManager;
import com.mugui.spring.net.classutil.DataSave;

import cn.hutool.core.exceptions.UtilException;
import cn.hutool.core.util.XmlUtil;

@Component
@AutoManager
public class GroupLockManager implements ManagerInterface<String,Object> {
	private static HashMap<String, Object> map = null;

	public void init() {
		clear();
		map = new HashMap<>();
		
	}

	public Set<Entry<String, Object>> entrySet() {
		return map.entrySet();
	}
	@Override
	public boolean init(Object object) {
		init();
		loadByDefaultConfigXml();
		loadByClass((Class<?>) object);
		return true;
	}

	private String name = null;
	private String value = null;

	private void loadByDefaultConfigXml() {
		Resource resource = new ClassPathResource("ForwardConfig.xml");
		try {
			Document document = null;
			try {
				document = XmlUtil.readXML(resource.getInputStream());
			} catch (Exception e) {
				return;
			}
			NodeList list = document.getChildNodes().item(0).getChildNodes();
			for (int i = 0; i < list.getLength(); i++) {
				Node item = list.item(i);
				if (item.getNodeName().equals("net-lock-group")) {
					NodeList childNodes = item.getChildNodes();
					for (int j = 0; j < childNodes.getLength(); j++) {
						if (childNodes.item(j).getNodeName().equals("name")) {
							name = childNodes.item(j).getChildNodes().item(0).getNodeValue();
						}
						if (childNodes.item(j).getNodeName().equals("value")) {
							value = childNodes.item(j).getChildNodes().item(0).getNodeValue();
						}
					}
					NetBagGroupLock forward = new NetBagGroupLock() {

						@Override
						public Class<? extends Annotation> annotationType() {
							return null;
						}

						@Override
						public String[] value() {
							return value.split(".");
						}

						@Override
						public String name() {
							return name;
						}
					};

					if (forward.value() == null)
						for (String key : forward.value()) {
							handleKey(key, forward.name());
						}

				}

			}
		} catch (UtilException e) {
			e.printStackTrace();
		}
	}

	private void loadByClass(Class<?> name) {
		for (Class<?> class_name : DataSave.initClassList(name)) {
			if (class_name.isAnnotationPresent(NetBagGroupLock.class)) {
				NetBagGroupLock filter = class_name.getAnnotation(NetBagGroupLock.class);
				if (filter.value() == null)
					for (String key : filter.value()) {
						handleKey(key, filter.name());
					}
			}
		}
	}

	private void handleKey(String key, String value) {
		String keys[] = key.split("[.]");
		HashMap<String, Object> tempmap = map;
		for (String temp : keys) {
			temp = temp.trim();
			HashMap<String, Object> t = null;
			if ((t = (HashMap<String, Object>) tempmap.get(temp)) == null) {
				t = new HashMap<>();
				tempmap.put(temp, t);
			}
			tempmap = t;
		}
		tempmap.put("&&", value);

	}

	@Override
	public boolean clear() {
		if (map != null)
			map.clear();
		return false;
	}

	@Override
	public boolean is(String name) {
		if (map == null) {
			throw new NullPointerException("please run init");
		}
		return !map.isEmpty() && map.get(name) != null;
	}

	@Override
	public Object del(String name) {
		if (map == null) {
			throw new NullPointerException("please run init");
		}
		return map.remove(name);
	}

	@Override
	public Object get(String name) {
		if (map == null) {
			throw new NullPointerException("please run init");
		}
		HashMap<String, Object> tempmap = map;
		for (String key : name.split("[.]")) {// TODO 如无意外，暂不用修改
			HashMap<String, Object> temp = (HashMap<String, Object>) tempmap.get(key);
			if (temp == null) {
				temp = (HashMap<String, Object>) tempmap.get("*");
			}
			if (temp == null) {
				return null;
			}
			tempmap = temp;
			if (tempmap.size() == 1 && temp.containsKey("&&")) {
				break;
			}
		}
		return tempmap.get("&&");
	}

	@Override
	public boolean add(String name, Object object) {
		if (map == null) {
			throw new NullPointerException("please run init");
		}
		return true;
//		NetBagGroupLockBean forwardBean = NetBagGroupLockBean.newBean(NetBagGroupLockBean.class, object);
//		NetBagGroupLock forward = new NetBagGroupLock() {
//
//			@Override
//			public Class<? extends Annotation> annotationType() {
//				return null;
//			}
//
//			@Override
//			public String[] value() {
//				return null;
//			}
//
//			@Override
//			public String name() {
//				return null;
//			}
//		};
//		for (String key : forward.value()) {
//			handleKey(key, forward);
//		}
	}

	@Override
	public boolean isInit() {
		// TODO Auto-generated method stub
		return false;
	}



	

}
