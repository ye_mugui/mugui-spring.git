package com.mugui.spring.net.classutil;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.context.ApplicationContext;

public class DataSave {
	public static List<Class<?>> CLASS_LIST = null;
	public static HashMap<Class<?>, Boolean> map = new HashMap<>();

	public static List<Class<?>> initClassList(Class<?> name) {
		if (map.get(name) != null)
			return CLASS_LIST;
		map.put(name, true);
//		CLASS_LIST = handle(name);
		CLASS_LIST=new ArrayList<Class<?>>();
		ApplicationContext applicationContext = (ApplicationContext) System.getProperties().get("Application");
		for (String type : applicationContext.getBeanDefinitionNames()) {
			CLASS_LIST.add(applicationContext.getType(type));
		}
//		CLASS_LIST.addAll(handle(DataSave.class));
		return CLASS_LIST;
	}

	public static List<Class<?>> handle(Class<?> name) {
		URL url = name.getClassLoader().getResource(name.getName().replaceAll("[.]", "/") + ".class");
		String type = url.getProtocol();
		List<Class<?>> list = new ArrayList<>();
		if (type.equals("file")) {
			list.addAll(ClassTool.getClassNameByFile(new File(url.getPath()).getParentFile(), null, name.getName()));
		} else if (type.equals("jar")) {
			list.addAll(ClassTool.getClassNameByJar(url.getPath(), name.getName()));
		}
		return list;
	}
}
