package com.mugui.spring.net.websocket;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.mugui.spring.net.task.TaskManager;

/**
 * WebSocket 任务配置注解
 * 
 * @auther 木鬼
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Inherited
public @interface WebSocket {
	/**
	 * 通过{@key}触发
	 */
	public static final int TRIGGER = 1;
	/**
	 * 循环操作 ,由于websocket的复杂性，不建议使用，请自身采用代码的方式进行循环<br>
	 * 参照 {@link TaskManager}
	 */
	@Deprecated
	public static final int CYCLE = 0;
	public static final int DEFAULT = TRIGGER;

	/**
	 * 类型 0 循环类型 1触发类型
	 * 
	 * 
	 */
	int type() default DEFAULT;

	/**
	 * 循环间隔,当为循环类型时这个参数才会生效
	 * 毫秒为单位
	 * 
	 */
	int blank() default 2000;

	/**
	 * 用于记录注解到某个类时，该类的标识
	 */
	String value() default "";
}
