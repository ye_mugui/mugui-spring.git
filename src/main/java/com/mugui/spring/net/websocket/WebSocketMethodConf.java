package com.mugui.spring.net.websocket;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
/**
 * WebSocket 返回数据的配置类
 * @author 木鬼
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Inherited
public @interface WebSocketMethodConf {
	/**
	 * 单结果可表示为所有调用者的得到结果
	 */
	public static final int ALL = 0;
	/**
	 * 单结果只表示调用者的结果
	 */
	public static final int ONE = 1;
	
	/**
	 * websocket 返回时结果适用范围
	 * @auther 木鬼
	 * @return
	 */
	int value() default ALL;
}
