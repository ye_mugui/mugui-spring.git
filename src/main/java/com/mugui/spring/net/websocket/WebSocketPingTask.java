package com.mugui.spring.net.websocket;

import javax.websocket.Session;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mugui.spring.base.Task;
import com.mugui.spring.base.TaskInterface;

@Component
@Task(blank = 10000, time = 0, value = Task.CYCLE)
public class WebSocketPingTask implements TaskInterface {

	public void init() {
		System.out.println(WebSocketPingTask.class.getName() + "启动");
	}

	@Autowired
	private WebSocketSessionManager webSocketSessionManager;
	
	@Autowired
	private WebSocketTask webSocketTask;
	
	@Override
	public void run() {
		for (Session session : webSocketSessionManager.values()) {
			if (!session.isOpen()) {
				try {
					session.close();
					webSocketSessionManager.del(session.getId());
					continue;
				} catch (Exception e) {
					continue;
				}
			}
			webSocketTask.sendData(session,"{\"ping\":" + System.currentTimeMillis() + "}",null,WebSocketTask.BIDA);
		}
	}

}
