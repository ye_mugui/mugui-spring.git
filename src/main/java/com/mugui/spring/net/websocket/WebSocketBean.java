package com.mugui.spring.net.websocket;

import com.alibaba.fastjson.JSONObject;
import com.mugui.bean.JsonBean;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * 该类为{@link WebSocket }注解的bean实现
 * 
 * @author 木鬼
 *
 */
@Getter
@Setter
@Accessors(chain = true)
public class WebSocketBean extends JsonBean {
	public WebSocketBean() {
		super();
	}

	public WebSocketBean(JSONObject object) {
		super(object);
	}

	public WebSocketBean(WebSocket filter, Class<?> class1) {
		this.type = filter.type();
		this.value = filter.value();
		this.blank = filter.blank();
		class_name = class1.getName();
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1968274181554792164L;

	/**
	 * 类型 0 循环类型 1触发类型
	 * 
	 * 
	 */
	private int type;

	/**
	 * 循环间隔,当为循环类型时这个参数才会生效
	 * 毫秒为单位
	 * 
	 */
	private int blank;

	/**
	 * 用于记录注解到某个类是，该类的标识
	 */
	private String value;
	/**
	 * 某类的class名称
	 */
	private String class_name;
	
	
	private WebSocketMethodConfBean conf; 
	
}
