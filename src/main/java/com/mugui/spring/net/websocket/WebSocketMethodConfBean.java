package com.mugui.spring.net.websocket;

import com.alibaba.fastjson.JSONObject;
import com.mugui.bean.JsonBean;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * 该类为{@link WebSocketMethodConf }注解的bean实现
 * 
 * @author 木鬼
 *
 */
@Getter
@Setter
@Accessors(chain = true)
public class WebSocketMethodConfBean extends JsonBean {
	/**
	 * 单结果可表示为所有调用者的得到结果
	 */
	public static final int ALL = 0;
	/**
	 * 单结果只表示调用者的结果
	 */
	public static final int ONE = 1;
	/**
	 * websocket 返回时结果适用范围
	 * 
	 * @auther 木鬼
	 * @return
	 */
	private int value;

	public WebSocketMethodConfBean() {
		super();
	}

	public WebSocketMethodConfBean(JSONObject object) {
		super(object);
	}

	public WebSocketMethodConfBean(WebSocketMethodConf filter) {
		this.value = filter.value();
	}
}
