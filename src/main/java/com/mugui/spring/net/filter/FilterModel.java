package com.mugui.spring.net.filter;

import com.mugui.Mugui;
import com.mugui.spring.net.bean.NetBag;

public interface FilterModel extends Mugui {
	public  NetBag filter(NetBag bag);
}
