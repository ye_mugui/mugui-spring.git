package com.mugui.spring.base;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Inherited
public @interface Cache {
	public String[] value() default "*";

	public int type() default POSITIVE;
	
	/**
	 * 	正向过滤，value匹配的将进入过滤器
	 */
	public int POSITIVE = 0;
	/**
	 * 	反向过滤，value不匹配的将进入过滤器
	 */
	public int REVERSE = 1;
}
