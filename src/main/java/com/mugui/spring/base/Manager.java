package com.mugui.spring.base;

import java.util.concurrent.ConcurrentHashMap;

public class Manager<K, V> implements ManagerInterface<K, V> {
	public ConcurrentHashMap<K, V> map = null;

	@Override
	public boolean init(Object object) {
		if (!isInit()) {
			synchronized (Manager.class) {
				if (!isInit()) {
					map = new ConcurrentHashMap<>();
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public boolean isInit() {
		return map != null;
	}

	@Override
	public V get(K key) {
		if (!isInit()) {
			throw new RuntimeException("not init");
		}
		return map.get(key);
	}

	@Override
	public boolean add(K key, V value) {
		if (!isInit()) {
			throw new RuntimeException("not init");
		}
		return map.put(key, value) != null;
	}

	@Override
	public boolean is(K key) {
		if (!isInit()) {
			throw new RuntimeException("not init");
		}
		return map.containsKey(key);
	}

	@Override
	public V del(K key) {
		if (!isInit()) {
			throw new RuntimeException("not init");
		}
		return map.remove(key);
	}

	@Override
	public boolean clear() {
		if (!isInit()) {
			throw new RuntimeException("not init");
		}
		map.clear();
		return true;

	}

}
