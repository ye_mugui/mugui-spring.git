package com.mugui.spring.base;



public interface ModelManagerInterface extends ManagerInterface<String,ModelInterface>{
	@Override
	ModelInterface del(String name);
	@Override
	ModelInterface get(String name);
	@Override
	boolean add(String name, ModelInterface object);
}
