package com.mugui.spring.base;

import com.mugui.Mugui;

/**
 * 一种简易的管理模型
 * 
 * @author 木鬼
 *
 * @param <K>
 * @param <V>
 */
public interface ManagerInterface<K, V> extends Mugui {
	boolean isInit();
	boolean init(Object object);
	V get(K key) ;
	boolean add(K key,V value) ;
	boolean is(K key) ;
	V del(K key);
	boolean clear();
}
