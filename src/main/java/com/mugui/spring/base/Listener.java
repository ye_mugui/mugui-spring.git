package com.mugui.spring.base;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.stereotype.Component;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Inherited
public @interface Listener {
	/**
	 * 	写入监听的路径
	 * @return
	 */
	public String[] value() default "*";

	public int type() default POSITIVE;
	public int weight() default 1;
	
	/**
	 * 	正向监听，value匹配的将进入监听器
	 */
	public int POSITIVE = 0;
	/**
	 * 	反向监听，value不匹配的将进入监听器
	 */
	public int REVERSE = 1;
}
