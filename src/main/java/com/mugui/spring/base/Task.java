package com.mugui.spring.base;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Inherited
public @interface Task {
	/**
	 * 设置该定时的类型
	 * 
	 * @return
	 */
	public int value() default DEFAULT;

	/**
	 * 设置多少毫秒之后执行该操作
	 * 
	 * @return
	 */
	public int time() default 500;

	/**
	 * 设置执行间隔，如value={@link SINGLE}为循环，则该设置生效
	 * 
	 * @return
	 */
	public int blank() default 5000;

	/**
	 * 单次操作，只执行一次的任务
	 */
	public static final int SINGLE = 0;
	public static final int DEFAULT = SINGLE;
	/**
	 * 循环操作，循环运行的任务
	 */
	public static final int CYCLE = 1;
}
