package com.mugui.spring.base;

import com.mugui.Mugui;

@Task
public interface TaskInterface extends Mugui {
	public void run();
}
