package com.mugui.spring.base;

import com.mugui.Mugui;

public abstract interface ModelInterface extends Mugui{
	
	public void init(); 

	public void start();

	public boolean isrun();

	public void stop();
	
}
