package com.mugui.spring.base;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.stereotype.Component;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Component
@Inherited
public @interface Module {
	public String name() ;
	public String type();
	public String GET="get";
	public String FIND="find";
	public String UPDATE="update";
	public String DELETE="delete";
	public String INSERT="insert";
	public String WEBSOCKET="ws";
	
}
